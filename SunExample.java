/*
 * R.Schneider
 * Multi threading, Omas Pl�tzchen
 * 28.01.2018
 * //git clone https://christopherpike@bitbucket.org/christopherpike/omas_plaetzchen.git
 * 
 * SunExample
 */

import java.util.*;
import java.io.*;
//package omas_plaetzchen;

class SunExample implements Runnable,Serializable
{
	
	public void run()
	{
		for(int i=0;i<100;i++)
		{
			if(i%10==0)
			{
				System.out.println("Every given tenth number. "+Thread.currentThread().getName());
			}
			
			try
			{
				Thread.sleep(100);
			}
			catch(InterruptedException exp)
			{
				System.out.println(exp);
			}
		
		}
		
		
		
	}
	
	
}
