/*
 * R.Schneider
 * Multi threading, Omas Pl�tzchen
 * 28.01.2018
 * //git clone https://christopherpike@bitbucket.org/christopherpike/omas_plaetzchen.git
 * // git commit -m "Initial commit" git push -u origin master      https://docs.oracle.com/javase/6/docs/
 * Main
 */

import java.util.*;
import java.io.*;
//package omas_plaetzchen;

class Kontobesitzer implements Runnable, Serializable
{
	private String name;
	
	Account konto=null;
	
	//---------
	
	public Kontobesitzer(String name)
	{
		this.name=name;
		this.konto=new Account();
		
	}
	
	// get /set
	
	//Name
	public String getName()
	{
		return this.name;
	}
	
	//Methoden
	
	public void geldabheben()
	{
		if(konto.getGuthaben()>10)
		{
			System.out.println(Thread.currentThread().getName()+" wird versuchen Geld anzuheben, nach einem \"Augenblick\" . Guthaben :"+konto.getGuthaben());
			
			try
			{
				Thread.sleep(500);
				
			}
			catch(InterruptedException pause)
			{
				System.
				out.println(pause);
			}
			
			konto.withdraw(10);
			System.out.println(Thread.currentThread().getName()+" hat Geld abgehoben.");
		}
		else
		{
			System.out.println(Thread.currentThread().getName()+" Verdammt!");
		}
	}
	
	public void run()
	{
		for(int i=0;i<5;i++)
		{
			
				geldabheben();
				if(konto.getGuthaben()<=0)
				{
					System.out.println(Thread.currentThread().getName()+" wollte Geld abheben, aber es war schon alle.");
				}
		}
		
	}
	
	
	//Overrides
	@Override
	public String toString()
	{
		return "Dieser Kontobesitzer hei�t "+getName()+".";
	}
}
