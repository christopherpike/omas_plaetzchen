/*
 * R.Schneider
 * Multi threading, Omas Pl�tzchen
 * 29.01.2018
 * //git clone https://christopherpike@bitbucket.org/christopherpike/omas_plaetzchen.git
 * // git commit -m "Initial commit" git push -u origin master      https://docs.oracle.com/javase/6/docs/
 * Main
 */

import java.util.*;
//package omas_plaetzchen;

class SyncTest implements Runnable
{
	private final int zahl;
	
	//-------
	
	public SyncTest()
	{
		this.zahl=5;
	}


	public void run()
	{
		System.out.println("Nicht synchronisiert.");
		
		synchronized(this)
		{
			System.out.println("Synchronisiert.");
		}
		
	}
	
	public synchronized void ausgabe()
	{
		System.out.println("Ausgabe, synchronisiert.");
	}

	// get / set ---------
	
	public int getZahl()
	{
		return this.zahl;
	}


	
}
