/*
 * R.Schneider
 * Multi threading, Omas Pl�tzchen
 * 24.01.2018
 * //git clone https://christopherpike@bitbucket.org/christopherpike/omas_plaetzchen.git
 * // git commit -m "Initial commit" git push -u origin master
 * Oma
 */

import java.util.*;
//import java.util.Random;

//package omas_plaetzchen;

class Oma implements Runnable
{
	private String name;
	
	private Random rd;
	
	// ------------
	
	public Oma(String name)
	{
		this.name=name;
	}
	
	// get / set -------------
	
	public String getName()
	{
		return this.name;
	}
	
	public void run()
	{
		while(Keksdose.getKekse()<5000)
		{
			backen();
		}
		
	}
	
	// Methoden
	
	public void backen()
	{
		Keksdose.fuelleKekse(30);
	}
}
