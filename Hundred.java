/*
 * R.Schneider
 * Multi threading, Omas Pl�tzchen
 * 29.01.2018
 * //git clone https://christopherpike@bitbucket.org/christopherpike/omas_plaetzchen.git
 * // git commit -m "Initial commit" git push -u origin master      https://docs.oracle.com/javase/6/docs/
 * Hundred
 */

import java.util.*;
//package omas_plaetzchen;

class Hundred implements Runnable
{
	public void run()
	{
		synchronized(Hundred.class)
		{
			int y=64;   //alt 65 97
			for(int i=0;i<30;i++)
			{
				
				if(i%10==0)
				{
					y++;
				}
				System.out.print((char)y+" "+Thread.currentThread().getName()+" ");
			}
		}
	}
}
