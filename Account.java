/*
 * R.Schneider
 * Multi threading, Omas Pl�tzchen
 * 28.01.2018
 * //git clone https://christopherpike@bitbucket.org/christopherpike/omas_plaetzchen.git
 * // git commit -m "Initial commit" git push -u origin master      https://docs.oracle.com/javase/6/docs/
 * Main
 */

import java.util.*;
//package omas_plaetzchen;

class Account
{
	//Balance
	private int guthaben=50;
	
	//Abfrage des Kontos
	public int getGuthaben()
	{
		return guthaben;
	}
	
	//Geldabheben
	public int withdraw(int betrag)
	{
		return guthaben-=betrag;
	}
}
